Client Api

To start client you must go to beatdev-ui folder and in terminal write npm start.
http://localhost:8000/ - UI 
http://localhost:8080/ - server

Server activity:

URL: "/user/addUser":

1. POST request can create new entity in database. it's should contains a new User in JSON format. Fields: Name(String), Email(String) and Image URL(String). This request returns id of new User

URL "/user/getUserInfo/{id}"

2. GET request returns information about user with id = {id}, format JSON.

URL "/user/changeUserStatus/{id}"

3. PUT request can change status of User with id={id}. Parameter: newStatus=[online|offline] returns JSON entity with following field: id, oldStatus, newStatus.

URL "/user/generate"

4. Generate prepared user