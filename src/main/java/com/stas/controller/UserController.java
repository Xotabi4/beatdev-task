package com.stas.controller;

import com.stas.common.UserStatus;
import com.stas.dto.StatusDTOResponse;
import com.stas.dto.UserDTORequest;
import com.stas.entity.User;
import com.stas.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import java.util.concurrent.TimeUnit;

@RestController
@CrossOrigin
@RequestMapping(value = "/user")
public class UserController {

    @Autowired
    private UserService userService;

    /**
     * Method is used to save a new User entity to database
     *
     * @param userDTORequest receive user as Json
     * @return RestResponse, contains an ID of the created user
     */

    @RequestMapping(value = "/addUser", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE)
    public RestResponse addUser(@RequestBody UserDTORequest userDTORequest) throws InterruptedException {
        User user = new User(userDTORequest.getName(), userDTORequest.getEmail(), userDTORequest.getImage());
        userService.save(user);
        TimeUnit.SECONDS.sleep(3);
        return RestResponse.ok(user.getId());
    }

    /**
     * This method returns an User information with assigned id
     *
     * @param id - id of user
     * @return RestResponse, contains user information or error message
     */

    @RequestMapping(value = "/getUserInfo/{id}", method = RequestMethod.GET)
    public RestResponse getUserInfo(@PathVariable("id") Long id) throws InterruptedException {
        User user = userService.getById(id);
        if(user == null)
            return RestResponse.error("Can't find user with this id");
        TimeUnit.SECONDS.sleep(3);
        return RestResponse.ok(userService.convertToDTO(user));
    }

    /**
     * The method changes the status of user,
     *
     * @param id      - to identify the user
     * @param newStatus - new status of user
     * @return RestResponse contains in ResponseBody
     * id - id of user,
     * oldStatus - old status,
     * newStatus - new status
     */

    @RequestMapping(value = "/changeUserStatus/{id}", method = RequestMethod.PUT, consumes = MediaType.APPLICATION_JSON_VALUE)
    public RestResponse changeUserStatus(@PathVariable("id") Long id, @RequestBody String newStatus) throws InterruptedException {
        if( !contains(newStatus) )
            return RestResponse.error("Incorrect status");
        User user = userService.getById(id);
        if(user == null)
            return RestResponse.error("Can't find user with this id");
        UserStatus oldStatus = user.getStatus();
        userService.changeStatus(user, UserStatus.valueOf(newStatus));
        TimeUnit.SECONDS.sleep(3);
        return RestResponse.ok(new StatusDTOResponse(oldStatus.toString(), user.getStatus().toString(), user.getId()));
    }

    private static boolean contains(String status){
        for(UserStatus us : UserStatus.values()){
            if(us.name().equals(status))
                return true;
        }
        return false;
    }

    @RequestMapping(value = "/generate")
    public String generate(){
        User user1 = new User("Robert", "Robert@robert.com", "https://upload.wikimedia.org/wikipedia/commons/thumb/9/94/Robert_Downey_Jr_2014_Comic_Con_%28cropped%29.jpg/220px-Robert_Downey_Jr_2014_Comic_Con_%28cropped%29.jpg");
        userService.save(user1);
        return "ok";
    }
}
