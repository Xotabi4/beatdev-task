package com.stas.controller;

public class RestResponse <PAYBACK, ERROR> {
    private final PAYBACK payback;
    private final ERROR error;

    RestResponse(PAYBACK payback, ERROR error) {
        this.payback = payback;
        this.error = error;
    }


    public static <ERROR> RestResponseError<ERROR> error(ERROR error) {
        return new RestResponseError<ERROR>(null, error);
    }



    public static <PAYBACK> RestResponseOk<PAYBACK> ok(PAYBACK payback) {
        return new RestResponseOk<PAYBACK>(payback, null);
    }

    public PAYBACK getPayback() {
        return payback;
    }

    public ERROR getError() {
        return error;
    }
}