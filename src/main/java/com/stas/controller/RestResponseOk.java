package com.stas.controller;

public class RestResponseOk<PAYBACK> extends RestResponse<PAYBACK, Object> {

    RestResponseOk(PAYBACK payback, Object o) {
        super(payback, o);
    }
}