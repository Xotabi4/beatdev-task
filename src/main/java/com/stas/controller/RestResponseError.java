package com.stas.controller;


public class RestResponseError<ERROR> extends RestResponse<Object, ERROR> {

    RestResponseError(Object o, ERROR error) {
        super(o, error);
    }
}
