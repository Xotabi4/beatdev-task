package com.stas.dto;


public class UserDTOResponse {
    private String name;
    private String image;
    private long id;
    private String status;
    private String email;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public UserDTOResponse() {

    }

    public UserDTOResponse(String name, String image, long id, String status, String email) {

        this.name = name;
        this.image = image;
        this.id = id;
        this.status = status;
        this.email = email;
    }
}
