package com.stas.dto;


public class StatusDTOResponse {
    private String oldStatus;
    private String newStatus;
    private long id;

    public String getOldStatus() {
        return oldStatus;
    }

    public void setOldStatus(String oldStatus) {
        this.oldStatus = oldStatus;
    }

    public String getNewStatus() {
        return newStatus;
    }

    public void setNewStatus(String newStatus) {
        this.newStatus = newStatus;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public StatusDTOResponse() {

    }

    public StatusDTOResponse(String oldStatus, String newStatus, long id) {

        this.oldStatus = oldStatus;
        this.newStatus = newStatus;
        this.id = id;
    }
}
