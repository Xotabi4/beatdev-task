package com.stas.service;

import com.stas.common.UserStatus;
import com.stas.dto.UserDTOResponse;
import com.stas.entity.User;
import com.stas.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


@Service
public class UserService {

    @Autowired
    private UserRepository userRepository;

    public User getById(Long id){
        return userRepository.findById(id);
    }

    public User save(User user){
        return userRepository.saveAndFlush(user);
    }

    public User changeStatus(User user, UserStatus status){
        user.setStatus(status);
        return userRepository.saveAndFlush(user);
    }

    public UserDTOResponse convertToDTO(User user){
        return new UserDTOResponse(user.getName(), user.getImage(), user.getId().intValue(), user.getStatus().toString(), user.getEmail());
    }
}
