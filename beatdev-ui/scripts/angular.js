angular.module("beatdevApp",[])
    .controller('UserController', function($scope, $http){
        $scope.loading = false;
        $scope.newUser={};
        $scope.findUserInfo={};
        $scope.changedStatusInfo={};
        $scope.add = function() {
            $scope.loading = true;
            $http.post("http://localhost:8080/user/addUser", $scope.newUser).then(function (result) {
                if(!result.data.error) {
                    $scope.newUserId = result.data.payback;
                }else{
                    alert(result.data.error);
                }
                $scope.newUser={};
                $scope.loading = false;
            })
        };
        $scope.getUserInfo = function(){
            $scope.loading = true;
            $http.get("http://localhost:8080/user/getUserInfo/" + $scope.findUserId).then(function(result){
                if(!result.data.error) {
                    $scope.findUserInfo = result.data.payback;
                }else{
                    alert(result.data.error);
                }
                $scope.findUserId = "";
                $scope.loading = false;
            })
        };
        $scope.changeUserStatus = function () {
            $scope.loading = true;
            $http.put("http://localhost:8080/user/changeUserStatus/" + $scope.changeUserStatusId, $scope.newStatus).then(function (result) {
                if(!result.data.error) {
                    $scope.changedStatusInfo = result.data.payback;
                }else {
                    alert(result.data.error);
                }
                $scope.changeUserStatusId = "";
                $scope.newStatus = "";
                $scope.loading = false;
            })
        }

    })
    .directive('loading', function() {
        return {
            restrict: 'E',
            template: '<div class="loading-overlay"><div class="loading"><img src="../../../loading.gif"/></div></div>',
            replace: true
        };
    });